# Torino Agile Conference 2018

[![pipeline status](https://gitlab.com/italian-agile-movement/mini2018torino/badges/master/pipeline.svg)](https://gitlab.com/italian-agile-movement/mini2018torino/commits/master)

Sito Torino Agile Conference, Torino, 3 febbraio 2018  
[https://agilemovement.it/venture/2018/torino/](https://agilemovement.it/venture/2018/torino/)

